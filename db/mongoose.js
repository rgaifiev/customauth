/**
 * Created by rgaifiev_usr on 13.06.2016.
 */

var mongoose = require('mongoose');
var config = require('../conf/config');
var logger = require('../logger')(module);

mongoose.connect( config.get('mongodb:url'), config.get('mongodb:options') );

mongoose.connection.on( 'connected', function() {
	logger.info('Mongoose connection open to', config.get('mongodb:url'));
});


module.exports = mongoose;
