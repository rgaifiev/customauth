/**
 * Created by rgaifiev_usr on 08.06.2016.
 */

var express = require('express');
var logger = require('../logger')(module);
var router = express.Router();
var User = require('../db/models/user').User;
var AuthError = require('../db/models/user').AuthError;
var HttpError = require('../error').HttpError;

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('login', { title: 'Test Authentication' });
});

router.post( '/login', function(req,res,next) {
	var username = req.body.username;
	var password = req.body.password;

	User.authorize( username, password, function(err, user ) {
		if( err ) {
			if( err instanceof AuthError ) {
				return next(new HttpError(403, err.message));
			} else {
				next(err);
			}
		}
		req.session.user = user._id;
		res.send({});
	});
});


module.exports = router;
