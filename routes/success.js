/**
 * Created by rgaifiev_usr on 13.06.2016.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('success', { message: 'You are logged in successfully' });
});

module.exports = router;
