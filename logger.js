/**
 * Created by rgaifiev_usr on 30.05.2016.
 */
var winston = require('winston');
const ENV = process.env.NODE_ENV;

function getLogger(module) {
	const path = module.filename.split(/[/\\]/).slice(-2).join('/');
	return new winston.Logger( {
		transports: [
			new winston.transports.Console( {
				colorize: true,
				level: ENV === 'development' ? 'debug' : 'error',
				label: path
			})
		]
	});
}

module.exports = getLogger;