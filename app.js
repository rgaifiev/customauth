var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cons = require('consolidate');
var config = require('./conf/config');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var mongoose = require('./db/mongoose');
var log = require('./logger')(module);
var util = require('util');
var http = require('http');
var HttpError = require('./error').HttpError;

var app = express();

/////////////////////////////////////////////////////////////////////////
// view engine setup
// I'm using DUST.js (best perfomance engine)
app.engine( 'dust', cons.dust );
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'dust');
/////////////////////////////////////////////////////////////////////////

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
var confSession = config.get('session');
confSession.store = new MongoStore( {mongooseConnection: mongoose.connection} );
app.use(session(confSession));

/////////////////////////////////////////////////////////////////////////
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/components', express.static(path.join(__dirname, './bower_components')));
/////////////////////////////////////////////////////////////////////////

app.use( require('./middleware/sendHttpError'));
app.use( require('./middleware/loadUser'));

/////////////////////////////////////////////////////////////////////////
// Rounting
app.use('/', require('./routes/login'));
app.use('/success', require('./routes/success'));
/////////////////////////////////////////////////////////////////////////

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
	if (typeof err == 'number') {
		err = new HttpError(err);
	}

	if (err instanceof HttpError) {
		res.sendHttpError(err);
	} else {
		log.info(err);
		err = new HttpError(500);
		res.sendHttpError(err);
	}
});

module.exports = app;
