/**
 * Created by rgaifiev_usr on 08.06.2016.
 */

const nconf = require('nconf');
const path = require('path');

nconf.argv()
	.env()
	.file( path.join(__dirname, 'config.json' ));

module.exports = nconf;